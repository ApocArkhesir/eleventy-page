# Readme
This is my static webpage code. This file should be ignored by eleventy and not rendered as a page template.

# Tech stack
It's just an npm project using @11ty/eleventy package. I'll probably have some sort of very simple CI/CD in GitLab along with hosting in GitLab pages and that's pretty much what I plan so far.